package api

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/galago.fun/galago.cli/models"
)

/*
function_name=$2
data=$3
curl -d "${data}" \
	-H "Content-Type: application/json" \
	-X POST "${url_api}/functions/call/${function_name}"

# with the version of the function

function_name=$2
function_version=$3
data=$4
curl -d "${data}" \
	-H "Content-Type: application/json" \
	-X POST "${url_api}/functions/call/${function_name}/${function_version}"
*/

//TODO: display status code
//TODO: return an error when code 500 (or not 200)

func CallWasmFunction(config models.GalagoConfig, functionName string, functionVersion string, jsonString string, jsonHeaders string) (int, string) {

	// get parameters
	bodyParams := bytes.NewReader([]byte(jsonString))

	// get headers
	headersMap := make(map[string]string)
	err := json.Unmarshal([]byte(jsonHeaders), &headersMap)
	if err != nil {
		panic(err)
	}

	//client := http.Client{}
	req, _ := http.NewRequest(http.MethodPost, getCallApiUrl(config, functionName, functionVersion), bodyParams)

	for key, value := range headersMap {
		req.Header.Add(key, value)
	}

	req.Header.Set("Content-Type", "application/json")

	return execPostRequest(req)

}