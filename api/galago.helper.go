package api

import (
	"io/ioutil"
	"net/http"
)

func execPostRequest(req *http.Request) (int,string) {
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	//fmt.Println("Status code:", resp.StatusCode) // bad 500 400 // good 200

	return resp.StatusCode, string(body[:])
}
