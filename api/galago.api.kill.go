package api

import (
	"net/http"

	"gitlab.com/galago.fun/galago.cli/models"
)


/*
function_name=$2
function_version=$3
process_index=$4

curl \
	-H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
	-X POST "${url_api}/functions/kill/${function_name}/${function_version}/${process_index}"
*/
func KillFunction(config models.GalagoConfig, functionName string, functionVersion string, processIndex int64) (int, string) {
	//bodyParams := bytes.NewReader([]byte(jsonString))

	req, _ := http.NewRequest(http.MethodPost, getKillApiUrl(config, functionName, functionVersion, processIndex), nil)

	req.Header.Add("admin_galago_token", config.Token)
	//req.Header.Set("Content-Type", "application/json")
	/*
		Body cannot be empty when content-type is set to 'application/json'
	*/
	return execPostRequest(req)

}

