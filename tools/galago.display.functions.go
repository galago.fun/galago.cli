package tools

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"

	"gitlab.com/galago.fun/galago.cli/models"
)

// https://www.sohamkamani.com/golang/parsing-json/
// parsing json
func GetFunctionsRecords(payload []byte) []models.GalaGoFunctionData {
	var functionsDataRecords []models.GalaGoFunctionData
	err := json.Unmarshal(payload, &functionsDataRecords)
	if err != nil {
		panic(err)
	}
	return functionsDataRecords
}

func DisplayFunctionData(functionsRecords []models.GalaGoFunctionData) {
	writer := new(tabwriter.Writer)
	// Format in tab-separated columns with a tab stop of 8.
	writer.Init(os.Stdout, 0, 8, 0, '\t', 0)
	fmt.Fprintln(writer, ".\tpid\tfunction\tversion\tindex\tactive\tdescription\tcounter\tstatus\tstart\tend\tduration")
	fmt.Fprintln(writer, ".\t---\t--------\t-------\t-----\t------\t-----------\t-------\t------\t-----\t---\t--------")

	for index, functionRecord := range functionsRecords {

		indexRow := strconv.Itoa(index)
		pid := strconv.FormatInt(functionRecord.Pid, 10)
		functionName := functionRecord.Function
		version := functionRecord.Version
		processIndex := strconv.FormatInt(functionRecord.ProcessIndex, 10)
		isActive := strconv.FormatBool(functionRecord.IsDefaultVersion)
		description := functionRecord.Description
		counter := strconv.FormatInt(functionRecord.Counter, 10)
		status := functionRecord.Status
		start := functionRecord.Started
		end := functionRecord.Ended
		duration := strconv.FormatInt(functionRecord.Duration, 10)

		row := indexRow + "\t" + pid + "\t" + functionName + "\t" + version + "\t" + processIndex + "\t" + isActive + "\t" + description + "\t" + counter + "\t" + status + "\t" + start + "\t" + end + "\t" + duration

		fmt.Fprintln(writer, row)
	}

	fmt.Fprintln(writer)
	writer.Flush()
}