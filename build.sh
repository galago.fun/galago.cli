#!/bin/bash
cli_name="galago"
GOOS=linux GOARCH=amd64 go build -o ./${cli_name}
chmod +x ./${cli_name}

sudo cp ${cli_name} /usr/local/bin
rm ${cli_name}

