package main

import (
	//"encoding/json"
	"flag"
	"fmt"
	"os"

	"gitlab.com/galago.fun/galago.cli/api"
	"gitlab.com/galago.fun/galago.cli/config"
	"gitlab.com/galago.fun/galago.cli/models"
	"gitlab.com/galago.fun/galago.cli/tools"
)

type Parameters struct {
	cmd          string
	name         string
	json         string
	ver          string
	wasm         string
	headers      string
	processIndex int64
	source       string
}

func main() {
 
	config, err := config.GetConfig()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	cmdPtr := flag.String("cmd", "", "execute a command, ex: `data`, `filter`, ...")
	namePtr := flag.String("name", "", "function name")
	jsonPtr := flag.String("json", "{}", "json string parameters")
	verPtr := flag.String("ver", "", "version of the function")
	wasmPtr := flag.String("wasm", "", "path to the wasm file")
	headersPtr := flag.String("headers", "{}", "request headers")
	processIndexPtr := flag.Int64("process", 0, "index of the process to kill")
	sourcePtr := flag.String("source", "", "path to the go file")

	flag.Parse()

	parameters := Parameters{
		*cmdPtr,
		*namePtr,
		*jsonPtr,
		*verPtr,
		*wasmPtr,
		*headersPtr,
		*processIndexPtr,
		*sourcePtr,
	}

	switch what := parameters.cmd; what {
	case "data":
		// go run main.go -cmd=data -name=hello
		// go run main.go -cmd=data
		payload := api.GetData(config, parameters.name)
		functionsRecords := tools.GetFunctionsRecords(payload)
		tools.DisplayFunctionData(functionsRecords)

	case "call":
		// go run main.go -cmd=call -name=hello -json='{"firstName":"Bob","lastName":"Morane"}'
		// go run main.go -cmd=call -name=hello -json='{"firstName":"Bob","lastName":"Morane"}' -ver=000
		// go run main.go -cmd=call -name=hey -json='{"firstName":"Bob","lastName":"Morane"}'

		// go run main.go -cmd=call -name=hi -json='{"name":"Bob"}' -headers='{"DEMO_TOKEN":"plop plop"}'
		// go run main.go -cmd=call -name=hi -json='{"name":"Bob"}' -headers='{"demo_token":"plouf"}'


		httpStatusCode, resultJsonString := api.CallWasmFunction(
			config, 
			parameters.name, 
			parameters.ver, 
			parameters.json, 
			parameters.headers,
		)

		result := models.GalagoFunctionResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value interface{}) {
				fmt.Printf("Success (function result): %s", value)
				fmt.Println()
				os.Exit(0)
			},
		)		


	case "deploy":
		// go run main.go -cmd=deploy -name=ping -wasm=samples/ping/ping.wasm -ver=0.0.0

		httpStatusCode, resultJsonString := api.DeployWasmFunction(
			config,
			parameters.name,
			parameters.ver,
			parameters.wasm,
		)
		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)		

	case "activate":
		// go run main.go -cmd=activate -name=ping -ver=004
		// go run main.go -cmd=activate -name=yo -ver=0.0.1

		// {"failure":null,"success":{"message":"activation is ok","executor":"yo","defaultVersion":"0.0.0","formerDefaultVersion":"0.0.1"}}
		// {"failure":"Cannot read property 'activateAsDefaultVersion' of undefined","success":null}

		httpStatusCode, resultJsonString := api.ActivateFunction(
			config,
			parameters.name,
			parameters.ver,
		)

		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)

	case "deactivate":
		// go run main.go -cmd=deactivate -name=ping -ver=004
		// go run main.go -cmd=deactivate -name=yo -ver=0.0.1

		httpStatusCode, resultJsonString := api.DeactivateFunction(
			config,
			parameters.name,
			parameters.ver,
		)

		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)

	case "scale":
		// go run main.go -cmd=scale -name=hi -ver=1.0.1
		// go run main.go -cmd=scale -name=hi -ver=1.0.0

		httpStatusCode, resultJsonString := api.ScaleFunction(
			config,
			parameters.name,
			parameters.ver,
		)
		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)

	case "remove":
		// go run main.go -cmd=remove -name=hi -ver=1.0.1
		// go run main.go -cmd=remove -name=hi -ver=1.0.0

		httpStatusCode, resultJsonString := api.RemoveFunction(
			config,
			parameters.name,
			parameters.ver,
		)
		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)

	case "kill":
		// go run main.go -cmd=kill -name=hi -ver=1.0.1 -process=1

		httpStatusCode, resultJsonString := api.KillFunction(
			config,
			parameters.name,
			parameters.ver,
			parameters.processIndex,
		)
		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)

	case "build":
		// go run main.go -cmd=build -name=yo -source=samples/yo/main.go -ver=0.0.2
		// go run main.go -cmd=activate -name=yo -ver=0.0.2
		// go run main.go -cmd=call -name=yo -json='{"name":"Jane Doe"}'

		httpStatusCode, resultJsonString := api.BuildWasmFunction(
			config,
			parameters.name,
			parameters.ver,
			parameters.source,
		)
		result := models.GalagoResult{}.OfJsonString(httpStatusCode, resultJsonString)

		result.Either(
			func(errorMessage string) {
				fmt.Printf("Failure: %s", errorMessage)
				fmt.Println()
				os.Exit(1)
			},
			func(value map[string]interface{}) {
				fmt.Printf("Success: %s", value["message"].(string))
				fmt.Println()
				os.Exit(0)
			},
		)		

	default:
		fmt.Println("🤔 same player shoot again")
	}

}
