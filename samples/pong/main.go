
package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {

	name := args[0].Get("name")

	return map[string]interface{}{
		"message": "🏓 ping " + name.String() + " 🤖",
	}
}

func main() {
	println("🤖: pong wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}
