
package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {

	name := args[0].Get("name")

	return map[string]interface{}{
		"message": "🖖 yo " + name.String() + " 👽",
	}
}

func main() {
	println("🤖: yo wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}
