/* [DOC]
## initializeWasm & asyncInitializeWasm

These 2 methods run the main function of a GoLang wasm file.

> - **Prerequisites**: load the wasm file `wasmFile = fs.readFileSync(wasmFilePath)`
> - **Remarks**:
>   - this helper is developped to work with nodejs
>   - usage of `WebAssembly.instantiate` instead of `WebAssembly.instantiateStreaming` (for the browser)

[DOC] */

(function(){

  function initializeWasm(wasmFile, args, options) {
    const go = new Go()
    // hack
    go.importObject.env["syscall/js.finalizeRef"] = () => {}
  
    // useful? (TODO: to be checked)
    if(options) go.importObject.env = options
  
    return new Promise((resolve, reject) => {
      // instantiate: nodejs
      // instantiateStreaming: browser
      WebAssembly.instantiate(wasmFile, go.importObject)
      .then(result => {
        if(args) go.argv = args // node supported by the wasm_exec.js version of TinyGO
        go.run(result.instance) 
        resolve(result.instance)
      })
      .catch(error => {
        reject(error)
      })
    })
  }

  // https://developers.google.com/web/updates/2018/04/loading-wasm
  async function asyncInitializeWasm(wasmFile, args, options) {
    const go = new Go()
    // hack
    go.importObject.env["syscall/js.finalizeRef"] = () => {}

    // useful? (TODO: to be checked)
    // could be interesting to call a jabascript function
    // what are the use cas ?
    if(options) go.importObject.env = options    

    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args // node supported by the wasm_exec.js version of TinyGO
    try {
      go.run(instance)
    } catch (error) {
      console.log("😡[wasm_helper.js]", error)
      throw error
    }
    
    
  }

  global.asyncInitializeWasm = asyncInitializeWasm
  global.initializeWasm = initializeWasm

})()
