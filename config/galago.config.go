package config

import (
	"errors"
	"os"
	"gitlab.com/galago.fun/galago.cli/models"

)

func GetConfig() (models.GalagoConfig, error) {

	galagoUrl, urlPresent := os.LookupEnv("GALAGO_URL") 	
	galagoToken, tokenPrensent := os.LookupEnv("GALAGO_TOKEN")

	if urlPresent && tokenPrensent {
		galagoConfig := models.GalagoConfig{
			Url: galagoUrl,
			Token: galagoToken,
			DataApi: "/functions/processes/data"      ,
			CallApi: "/functions/call"      ,
			DeployApi: "/functions/publish"   ,
			ActivateApi: "/functions/activate"  ,
			DeactivateApi: "/functions/deactivate",
			ScaleApi: "/functions/scale"     ,
			RemoveApi:"/functions/remove"     ,
			KillApi: "/functions/kill"      ,
			BuildApi: "/functions/build"     ,
		}
		return galagoConfig, nil
	} else {
		return models.GalagoConfig{}, errors.New("You need to set GALAGO_URL and GALAGO_TOKEN")
	}

}